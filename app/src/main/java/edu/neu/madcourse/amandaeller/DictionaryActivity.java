package edu.neu.madcourse.amandaeller;

import android.app.Activity;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;

import java.util.Set;

/**
 * Created by Amanda on 1/31/2017.
 */
public class DictionaryActivity extends Activity {
//  public static Set<String> res = new HashSet<>();
//  public String[] set;
  public static ArrayList<String> res = new ArrayList<>();
  public ArrayAdapter<String> resAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dictionary);
    setList();
    dictionaryWatcher();
  }

  public void dictionaryWatcher(){
    EditText searchWord = (EditText) findViewById(R.id.dictionarySearch);

    searchWord.addTextChangedListener(new TextWatcher() {
      EditText input = (EditText) findViewById(R.id.dictionarySearch);

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(MainActivity.dic != null) {
          String foo = input.getText().toString();
          if(MainActivity.dic.contains(foo)){
            ToneGenerator tone = new ToneGenerator(AudioManager.STREAM_MUSIC, 50);
            tone.startTone(ToneGenerator.TONE_PROP_BEEP, 170);
            if(!res.contains(foo)) {
              res.add(foo);
              setList();
            }
          }
        }
      }

      @Override
      public void afterTextChanged(Editable s) {}
    });
  }

  public void setList(){
    if(res != null) {
     // set = res.toArray(new String[0]);
      resAdapter = new ArrayAdapter<String>(DictionaryActivity.this, android.R.layout.simple_list_item_1, res);//set);
      ListView listView = (ListView) findViewById(R.id.dictionaryResults);
      listView.setAdapter(resAdapter);
    }
  }

}
