package edu.neu.madcourse.amandaeller;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Created by Amanda on 1/31/2017.
 */

public class DictionaryFragment extends Fragment {

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_dictionary, container, false);
    // Handle buttons here...

    View menuButton = rootView.findViewById(R.id.dictionary_menu_button);
    View clearButton = rootView.findViewById(R.id.clear_button);
    View acknowledgeButton = rootView.findViewById(R.id.scrog_acknowledgement_button);

    menuButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        getActivity().startActivity(intent);
      }
    });

    clearButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(DictionaryActivity.res != null){
          DictionaryActivity.res.clear();
         // String[] set = DictionaryActivity.res.toArray(new String[0]);
          ArrayAdapter resAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, DictionaryActivity.res);//set);
          ListView listView = (ListView) getView().findViewById(R.id.dictionaryResults);
          listView.setAdapter(resAdapter);
        }
        EditText editText = (EditText) getView().findViewById(R.id.dictionarySearch);
        editText.setText("");
      }
    });

    acknowledgeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.acknowledgement_title);
        builder.setMessage(R.string.dic_acknowledgement_content);
        builder.show();
      }
    });

    return rootView;
  }

}
