package edu.neu.madcourse.amandaeller;

/**
 * Created by Amanda on 1/11/2017.
 */

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.amandaeller.database.DatabaseActivity;
import edu.neu.madcourse.amandaeller.scroggle.GameActivity;
import edu.neu.madcourse.amandaeller.slowLoris.RotationVectorActivity;

public class MainFragment extends Fragment {
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_main, container, false);
    // Handle buttons here...

    View errorButton = rootView.findViewById(R.id.error_button);
    View aboutButton = rootView.findViewById(R.id.about_button);
    View dictionaryButton = rootView.findViewById(R.id.dictionary_button);
    View wordGameButton = rootView.findViewById(R.id.scroggle_button);
    View leaderboardButton = rootView.findViewById(R.id.leaderboard_button);
    View scoreboardButton = rootView.findViewById(R.id.scoreboard_button);
    View slowLorisButton = rootView.findViewById(R.id.slowLoris_button);

    aboutButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), AboutActivity.class);
        getActivity().startActivity(intent);
      }
    });

    errorButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), ErrorActiviy.class);
        getActivity().startActivity(intent);
      }
    });

    dictionaryButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), DictionaryActivity.class);
        getActivity().startActivity(intent);
      }
    });

    wordGameButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), GameActivity.class);
        getActivity().startActivity(intent);
      }
    });

    leaderboardButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        DatabaseActivity.isLeaderBoard = true;
        Intent intent = new Intent(getActivity(), DatabaseActivity.class);
        getActivity().startActivity(intent);
      }
    });
    scoreboardButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        DatabaseActivity.isLeaderBoard = false;
        Intent intent = new Intent(getActivity(), DatabaseActivity.class);
        getActivity().startActivity(intent);
      }
    });
    slowLorisButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), RotationVectorActivity.class);
        getActivity().startActivity(intent);
      }
    });

    return rootView;
  }

}
