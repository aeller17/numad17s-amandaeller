/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.amandaeller.scroggle;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.amandaeller.R;
import edu.neu.madcourse.amandaeller.database.DatabaseActivity;

public class ControlFragment extends Fragment {
  private static CountDownTimer timer;
  public static int ticsLeft;
  public static int tic = 1000;
  public static boolean paused = false;
  public static TextView score;
  public static Button p2submit;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    final View rootView =
        inflater.inflate(R.layout.fragment_control, container, false);
    View main = rootView.findViewById(R.id.button_main);
    View restart = rootView.findViewById(R.id.button_restart);
    View phase2 = rootView.findViewById(R.id.button_phase2_submit);
    View pause = rootView.findViewById(R.id.button_pause_resume);
    View ack = rootView.findViewById(R.id.scrog_acknowledgement_button);
    View leaderBoard = rootView.findViewById(R.id.LeaderBoard_button);
    View scoreBoard = rootView.findViewById(R.id.ScoreBoard_button);
    View instruction = rootView.findViewById(R.id.instruction_button);

    final View mute = rootView.findViewById(R.id.mute_sound);
    score = (TextView) rootView.findViewById(R.id.score);
    p2submit = (Button) rootView.findViewById(R.id.button_phase2_submit);

    ticsLeft = 181;
    setScore();
    startTimer(rootView);

    main.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        getActivity().finish();
      }
    });

    restart.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        ticsLeft = 181;
        paused = false;
        ((GameActivity) getActivity()).restartGame();
        startTimer(rootView);
        setSubmit();
        setScore();
      }
    });

    pause.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        TextView bt = (TextView) rootView.findViewById(R.id.button_pause_resume);
        if (paused) {
          paused = false;
          startTimer(rootView);
          bt.setText("Pause");
        } else {
          timer.cancel();
          paused = true;
          bt.setText("Resume");
        }
        ((GameActivity) getActivity()).setAllColors();
      }
    });

    phase2.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        setSubmit();
        ((GameActivity) getActivity()).phase2();
      }
    });
    ack.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.acknowledgement_title);
        builder.setMessage(R.string.scrog_acknowledgement_content);
        builder.show();
      }
    });

    leaderBoard.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DatabaseActivity.isLeaderBoard = true;
        Intent intent = new Intent(getActivity(), DatabaseActivity.class);
        getActivity().startActivity(intent);
      }
    });

    scoreBoard.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DatabaseActivity.isLeaderBoard = false;
        Intent intent = new Intent(getActivity(), DatabaseActivity.class);
        getActivity().startActivity(intent);
      }
    });

    instruction.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getActivity(), InstructionActivity.class);
        getActivity().startActivity(intent);
      }
    });

    mute.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {GameActivity.onMute();}});
    return rootView;
  }


  private void startTimer(View rootview) {
    if(timer != null){timer.cancel();}
    final TextView tim = (TextView) rootview.findViewById(R.id.timer);
    timer = new CountDownTimer(ticsLeft * tic, tic) {

      @Override
      public void onTick(long millisUntilFinished) {
        int sec = (int) (millisUntilFinished % (tic * 60)) / tic;
        int min = (int) millisUntilFinished / (tic * 60);
        String secs = (sec < 10) ? "0" + sec : "" + sec;
        tim.setText("Time Left: " + min + ":" + secs);
        ticsLeft = (int) (millisUntilFinished / tic);

        if (ticsLeft < 90 && !GameFragment.inPhase2) {
          GameActivity ga = (GameActivity) getActivity();
          if(ga != null){ga.phase2();}
        }

        if (ticsLeft < 100 && ticsLeft >= 90 && !GameFragment.inPhase2) {
          tim.setBackgroundColor(Color.RED);
        } else if (ticsLeft < 10) {
          tim.setBackgroundColor(Color.RED);
        } else {
          tim.setBackgroundColor(Color.TRANSPARENT);
        }
      }
      @Override
      public void onFinish() {
        tim.setBackgroundColor(Color.TRANSPARENT);
        tim.setText("Game Over");
        paused = true;
        GameActivity ga = (GameActivity) getActivity();
        if(ga != null){ga.endGame();}
      }
    };
    timer.start();
  }

  public static void setScore() {
    score.setText("Score: " + GameFragment.score);
  }

  public static void setSubmit(){
    if(GameFragment.inPhase2) {
      p2submit.setText("Submit word");
    } else {
      p2submit.setText("Phase 2");
    }
  }
}
