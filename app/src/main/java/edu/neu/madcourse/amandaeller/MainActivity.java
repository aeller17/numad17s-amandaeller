package edu.neu.madcourse.amandaeller;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;

import edu.neu.madcourse.amandaeller.database.DatabaseActivity;
import edu.neu.madcourse.amandaeller.database.GameScore;
import edu.neu.madcourse.amandaeller.scroggle.GameActivity;

public class MainActivity extends Activity {
  private static final int PERMISSION_REQUEST_ON_PHONE_STATE = 3;
  public static HashSet<String> dic;
  public static ArrayList<String> scoggleDic;
  public static int[] patterns;
  private DatabaseReference db;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_ON_PHONE_STATE);
    }

    if (dic == null) {
      fileReader();
    }
    readUserLists();
    subscribeToNews();

    db = FirebaseDatabase.getInstance().getReference();
    db.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        DatabaseActivity.pushNotification();
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
      }
    });
  }

  private void readUserLists() {
    String wordScores = "WordScores";
    String gameScores = "GameScores";

    FileInputStream fip;
    try {
      fip = getApplicationContext().openFileInput(gameScores);
      ObjectInputStream ois = new ObjectInputStream(fip);

      GameActivity.userScoreList = (GameScore.ScoreBoard) ois.readObject();
      ois.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    try {
      fip = getApplicationContext().openFileInput(wordScores);
      ObjectInputStream ois = new ObjectInputStream(fip);

      GameActivity.userWordList = (GameScore.ScoreBoard) ois.readObject();
      ois.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public void subscribeToNews() {
    FirebaseMessaging.getInstance().subscribeToTopic("ScroggleTop10");
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {
      case PERMISSION_REQUEST_ON_PHONE_STATE: {
        if (grantResults.length > 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
        }
      }
    }
    return;
  }

  /*
  * FileReader deserialize the HashSet that makes up the dictionary words upon first start up,
  * once it's loaded into memory it runs smooth but the first time reading it does take some time
  * Wasn't sure how to make it go quicker
  */
  public void fileReader() {
    AssetManager assetManager = getAssets();
    InputStream input;
    if (dic == null) {
      try {
        input = assetManager.open("wordlist.dic");
        ObjectInputStream ois = new ObjectInputStream(input);

        dic = (HashSet<String>) ois.readObject();

        ois.close();

      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    if (scoggleDic == null) {
      try {
        input = assetManager.open("scrogglewordlist.dic");
        ObjectInputStream ois = new ObjectInputStream(input);

        System.out.println("reading in scoggle dic");
        scoggleDic = (ArrayList<String>) ois.readObject();

        ois.close();

      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    if (patterns == null) {
      try {
        input = assetManager.open("patterns.dat");
        ObjectInputStream ois = new ObjectInputStream(input);

        patterns = (int[]) ois.readObject();

        ois.close();

      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
  }

}
