package edu.neu.madcourse.amandaeller.slowLoris;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import edu.neu.madcourse.amandaeller.R;

import java.util.List;

public class RotationVectorActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;

    private final float[] mAccelMatrix = new float[3];

    private Sensor mLinearAccSensor;

    private AccelView mAccelView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get an instance of the SensorManager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        sensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        Log.d("SensorActivity", mSensorManager.getSensorList(Sensor.TYPE_ALL).toString());

        // find the rotation-vector sensor
        mLinearAccSensor = mSensorManager.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER);

        // initialize the rotation matrix to identity
        mAccelMatrix[0] = 0;
        mAccelMatrix[1] = 0;
        mAccelMatrix[2] = 0;

        setContentView(R.layout.activity_rotation_vector);

        mAccelView = (AccelView)findViewById(R.id.sensor_accel_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mLinearAccSensor, 50000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            mAccelMatrix[0] = event.values[0];
            mAccelMatrix[1] = event.values[1];
            mAccelMatrix[2] = event.values[2];

            mAccelView.setAccelMatrix(mAccelMatrix);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
