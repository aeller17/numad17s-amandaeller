package edu.neu.madcourse.amandaeller.database;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by aniru on 2/18/2017.
 */
@IgnoreExtraProperties
public class GameScore implements Serializable{
  public String username;
  public String score;
  public String datePlayed;
  public String bestWord;
  public String wordScore;

  public GameScore() {
  }  // Default constructor required for calls to DataSnapshot.getValue(GameScore.class)

  public GameScore(String username, String score, String bestWord, String wordScore) {
    this.username = username;
    this.score = score;
    this.bestWord = bestWord;
    this.wordScore = wordScore;
    this.datePlayed = new SimpleDateFormat("MM.dd.yy").format(new Date());
  }

  public static Comparator<GameScore> GScompare = new Comparator<GameScore>()   {
    @Override
    public int compare(GameScore o1, GameScore o2) {
      return Integer.valueOf(o2.score) - Integer.valueOf(o1.score);
    }
  };

  public static Comparator<GameScore> WScompare = new Comparator<GameScore>() {
    @Override
    public int compare(GameScore o1, GameScore o2) {
      return Integer.valueOf(o2.wordScore) - Integer.valueOf(o1.wordScore);
    }
  };

  public static class ScoreBoard extends ArrayList<GameScore>  implements Serializable{
    private int max = 10;
    public int beat = 0;
    private boolean word;

    public ScoreBoard(String comp){
      word = (comp.equals("word"));
    }

    @Override
    public boolean add(GameScore gs) {
      String v = (word) ? gs.wordScore : gs.score;
      if (Integer.valueOf(v) > beat || size() < max) {
        super.add(gs);
        Collections.sort(this, word? WScompare:GScompare);
        if (size() > max) {
          this.remove(max);
          beat = word ? Integer.valueOf(get(max - 1).wordScore) : Integer.valueOf(get(max - 1).score);
        }
        return true;
      }
      return false;
    }

    public boolean willAdd(GameScore gs) {
      String v = (word) ? gs.wordScore : gs.score;
      return (Integer.valueOf(v) > beat || size() < max);
    }
  }
}
