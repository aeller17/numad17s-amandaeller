/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
***/
package edu.neu.madcourse.amandaeller.scroggle;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;

import edu.neu.madcourse.amandaeller.R;

public class Tile {

   public enum Owner {
      X, O /* letter O */, NEITHER, BOTH
   }

   // These levels are defined in the drawable definitions
   private static final int LEVEL_X = 0;
   private static final int LEVEL_O = 1; // letter O
   private static final int LEVEL_BLANK = 2;
   private static final int LEVEL_AVAILABLE = 3;
   private static final int LEVEL_TIE = 3;

   private final GameFragment mGame;
   private Owner mOwner = Owner.NEITHER;
   private View mView;
   private Tile mSubTiles[];

   public Tile(GameFragment game) {
      this.mGame = game;
   }

   public View getView() {
      return mView;
   }

   public void setView(View view) {
      this.mView = view;
   }

   public Owner getOwner() {
      return mOwner;
   }

   public void setOwner(Owner owner) {
      this.mOwner = owner;
   }


   public void setSubTiles(Tile[] subTiles) {
      this.mSubTiles = subTiles;
   }

   public void updateDrawableState() {
      if (mView == null) return;
      int level = getLevel();
      if (mView.getBackground() != null) {
         mView.getBackground().setLevel(level);
      }
      if (mView instanceof ImageButton) {
         Drawable drawable = ((ImageButton) mView).getDrawable();
         drawable.setLevel(level);
      }
   }

   private int getLevel() {
      int level = LEVEL_BLANK;
      switch (mOwner) {
         case X:
            level = LEVEL_X;
            break;
         case O: // letter O
            level = LEVEL_O;
            break;
         case BOTH:
            level = LEVEL_TIE;
            break;
         case NEITHER:
            level = mGame.isAvailable(this) ? LEVEL_AVAILABLE : LEVEL_BLANK;
            break;
      }
      return level;
   }

   public void animate() {
      Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
            R.animator.tictactoe);
      if (getView() != null) {
         anim.setTarget(getView());
         anim.start();
      }
   }
}
