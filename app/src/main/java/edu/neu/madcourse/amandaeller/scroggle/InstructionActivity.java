package edu.neu.madcourse.amandaeller.scroggle;

import android.app.Activity;
import android.os.Bundle;

import edu.neu.madcourse.amandaeller.R;

/**
 * Created by Amanda on 3/11/2017.
 */
public class InstructionActivity  extends Activity{
  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_instructions);
  }
}
