package edu.neu.madcourse.amandaeller;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Amanda on 1/17/2017.
 */
public class AboutActivity extends Activity{

  @Override
  protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
  }

}
