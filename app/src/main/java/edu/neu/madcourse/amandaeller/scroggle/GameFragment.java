/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.amandaeller.scroggle;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import edu.neu.madcourse.amandaeller.R;
import edu.neu.madcourse.amandaeller.database.GameScore;

import static edu.neu.madcourse.amandaeller.MainActivity.dic;
import static edu.neu.madcourse.amandaeller.MainActivity.patterns;
import static edu.neu.madcourse.amandaeller.MainActivity.scoggleDic;

public class GameFragment extends Fragment {
  static private int mLargeIds[] = {R.id.large1, R.id.large2, R.id.large3,
      R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
      R.id.large9,};
  static private int mSmallIds[] = {R.id.small1, R.id.small2, R.id.small3,
      R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
      R.id.small9,};
  private Tile mEntireBoard = new Tile(this);
  private Tile mLargeTiles[] = new Tile[9];
  private Tile mSmallTiles[][] = new Tile[9][9];
  private Set<Tile> mAvailable = new HashSet<Tile>();
  private int mSoundX, mSoundO, mSoundMiss, mSoundRewind;
  private SoundPool mSoundPool;
  private float mVolume = 1f;
  private int mLastLarge;
  private int mLastSmall;

  private static int[] arr = new int[9];
  private Random rnd = new Random();
  private String[] theWords = new String[9];
  private int EMPTY = -1;
  private int[][] selected = new int[9][9];
  private int[] nSelected = new int[9];
  private WordScore[] ws = new WordScore[9];
  private WordScore ws2 = new WordScore(); //phase 2 word score
  private boolean[] submitted = new boolean[9];
  private char[][] daLetters = new char[9][9];
  public static boolean inPhase2 = false;
  private ArrayList<Point> P2 = new ArrayList<>(); //for phase 2, holds the fLarge and fSmall's that are still available
  private static int[][] adj = {{1, 3, 4}, {0, 2, 3, 4, 5,}, {1, 4, 5}, {0, 1, 4, 6, 7}, {0, 1, 2, 3, 5, 6, 7, 8}, {1, 2, 4, 7, 8}, {3, 4, 7}, {3, 4, 5, 6, 8}, {4, 5, 7}};
  public static int score;
  public static String bestWord = "";
  public static int bestWordScore = 0;
  public static String name = "";
  public static Set<String> wordSet = new HashSet<>();
  public static int[] letterScores = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Retain this fragment across configuration changes.
    setRetainInstance(true);
    initGame();
    mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
    mSoundX = mSoundPool.load(getActivity(), R.raw.sergenious_movex, 1);
    mSoundO = mSoundPool.load(getActivity(), R.raw.sergenious_moveo, 1);
    mSoundMiss = mSoundPool.load(getActivity(), R.raw.erkanozan_miss, 1);
    mSoundRewind = mSoundPool.load(getActivity(), R.raw.joanne_rewind, 1);
  }

  private void clearAvailable() {
    mAvailable.clear();
  }

  private void addAvailable(Tile tile) {
    tile.animate();
    mAvailable.add(tile);
  }

  public boolean isAvailable(Tile tile) {
    return mAvailable.contains(tile);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.large_board, container, false);
    initViews(rootView);
    updateAllTiles();
    return rootView;
  }

  private void intToArr(int k) {
    for (int i = 0; i < 9; i++) {
      arr[i] = k % 10;
      k = k / 10;
    }
  }

  private int getRandomPattern() {
    return patterns[rnd.nextInt(patterns.length)];
  }

  private void initViews(final View rootView) {
    mEntireBoard.setView(rootView);
    for (int large = 0; large < 9; large++) {
      View outer = rootView.findViewById(mLargeIds[large]);
      mLargeTiles[large].setView(outer);
      intToArr(getRandomPattern());
      String word = scoggleDic.get(rnd.nextInt(scoggleDic.size()));
      theWords[large] = word;

      for (int small = 0; small < 9; small++) {
        final Button inner = (Button) outer.findViewById
            (mSmallIds[small]);
        final int fLarge = large;
        final int fSmall = small;
        final Tile smallTile = mSmallTiles[large][small];
        smallTile.setView(inner);

        inner.setText("" + word.charAt(arr[small]));
        daLetters[fLarge][fSmall] = word.charAt(arr[small]);

        inner.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!ControlFragment.paused) {
              if (!inPhase2) { //phase 1
                if (!submitted[fLarge]) {
                  smallTile.animate();
                  if (nSelected[fLarge] >= 3 && selected[fLarge][fSmall] == 0) {
                    submitWord(fLarge);
                  } else if (legalEmpty(fLarge, fSmall)) {
                    inner.setBackgroundColor(propColor(fLarge, fSmall));
                    mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                  } else if (lastSelected(fLarge, fSmall)) {
                    inner.setBackgroundColor(propColor(fLarge, fSmall));
                    mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                  } else {
                    mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                  }
                }
              } else { //phase 2
                if (selected[fLarge][fSmall] != EMPTY && (P2.isEmpty() || isAdj(P2.get(P2.size() - 1).x, fLarge))) {
                  P2.add(new Point(fLarge, fSmall));
                  smallTile.animate();
                  inner.setBackgroundColor(propColor(fLarge, fSmall));
                }
              }
            }
          }
        });
      }
    }
  }

  private void submitWord(int fLarge) {
    WordScore w = ws[fLarge];
    for (int i = 0; i < nSelected[fLarge]; i++) {
      int j = find(i, selected[fLarge]);
      char c = daLetters[fLarge][j];
      w.addLetter(c);
    }
    score += w.getScore(wordSet);
    w.show();
    if (w.getScore(wordSet) != 0) {
      submitted[fLarge] = true;
      int i = 0;
      while (i < 9 && submitted[i]) {
        i++;
      }
      if (i == 9) {
        phase2();
      }
    } else {
      w.clear();
      for (int i = 0; i < 9; i++) {
        selected[fLarge][i] = EMPTY;
      }
      nSelected[fLarge] = 0;
    }
    setAllColors();
  }

  private int find(int k, int[] a) {
    for (int i = 0; i < 9; i++) {
      if (a[i] == k) {
        return i;
      }
    }
    return -1;
  }

  private int propColor(int fLarge, int fSmall) {
    if (ControlFragment.paused) {
      return Color.BLACK;
    }
    if (!inPhase2) {
      if (selected[fLarge][fSmall] != EMPTY) {
        return submitted[fLarge] ? Color.GREEN : Color.BLUE;
      } else {
        return submitted[fLarge] ? Color.BLACK : Color.LTGRAY;
      }
    } else {
      if (selected[fLarge][fSmall] == EMPTY) {
        return Color.BLACK;
      }
      for (Point p : P2) {
        if (p.x == fLarge && p.y == fSmall) {
          return Color.GREEN;
        }
      }
      return Color.LTGRAY;
    }
  }

  private boolean lastSelected(int fLarge, int fSmall) {
    int last = findLocLast(fLarge);
    if (last == fSmall) {
      selected[fLarge][fSmall] = EMPTY;
      nSelected[fLarge]--;
      return true;
    } else {
      return false;
    }
  }

  private boolean legalEmpty(int fLarge, int fSmall) {
    if (nSelected[fLarge] == 0) {
      selected[fLarge][fSmall] = 0;
      nSelected[fLarge] = 1;
      return true;
    }
    if (nSelected[fLarge] == 9) {
      return false;
    }
    int l = findLocLast(fLarge);
    if (isAdj(fSmall, l) && selected[fLarge][fSmall] == EMPTY) {
      selected[fLarge][fSmall] = nSelected[fLarge];
      nSelected[fLarge]++;
      return true;
    } else {
      return false;
    }
  }

  private boolean isAdj(int fSmall, int l) {
    for (int i : adj[fSmall]) {
      if (l == i) {
        return true;
      }
    }
    return false;
  }

  private int findLocLast(int fLarge) {
    int i;
    for (i = 0; i < 9; i++) {
      if (selected[fLarge][i] == (nSelected[fLarge] - 1)) { //found location of the last letter selected
        break;
      }
    }
    return i;
  }

  public void setAllColors() {
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        Tile t = mSmallTiles[i][j];
        t.getView().setBackgroundColor(propColor(i, j));
      }
    }
  }

  public void phase2() {
    inPhase2 = true;
    ControlFragment.setSubmit();
    for (int i = 0; i < 9; i++) {
      if (!submitted[i]) {
        for (int j = 0; j < 9; j++) {
          selected[i][j] = EMPTY;
        }
        nSelected[i] = 0;
      }
    }
    ws2.clear();
    for (Point p : P2) {
      ws2.addLetter(daLetters[p.x][p.y]);
    }
    score += ws2.getScore(wordSet);
    wordSet.add(ws2.word);
    ws2.show();
    P2.clear();
    setAllColors();
  }

  public void restartGame() {
    mSoundPool.play(mSoundRewind, mVolume, mVolume, 1, 0, 1f);
    inPhase2 = false;
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        Tile t = mSmallTiles[i][j];
        t.getView().setBackgroundColor(Color.LTGRAY);
      }
    }
    initGame();
    initViews(getView());
    updateAllTiles();
  }

  public void endGame() {
    setAllColors();
    GameActivity.finalScore = new GameScore(name, String.valueOf(score), bestWord, String.valueOf(bestWordScore));
  }

  public void initGame() {
    mEntireBoard = new Tile(this);
    // Create all the tiles
    for (int large = 0; large < 9; large++) {
      mLargeTiles[large] = new Tile(this);
      for (int small = 0; small < 9; small++) {
        mSmallTiles[large][small] = new Tile(this);
      }
      mLargeTiles[large].setSubTiles(mSmallTiles[large]);
    }
    mEntireBoard.setSubTiles(mLargeTiles);
    for (int i = 0; i < 9; i++) {
      for (int j = 0; j < 9; j++) {
        selected[i][j] = EMPTY;
      }
    }
    for (int i = 0; i < 9; i++) {
      nSelected[i] = 0;
      submitted[i] = false;
      ws[i] = new WordScore();
    }
    P2.clear();
    wordSet.clear();
    score = 0;
    bestWord = "";
    bestWordScore = 0;
    // If the player moves first, set which spots are available
    mLastSmall = -1;
    mLastLarge = -1;
  }

  private void setAvailableFromLastMove(int small) {
    clearAvailable();
    // Make all the tiles at the destination available
    if (small != -1) {
      for (int dest = 0; dest < 9; dest++) {
        Tile tile = mSmallTiles[small][dest];
        if (tile.getOwner() == Tile.Owner.NEITHER)
          addAvailable(tile);
      }
    }
    // If there were none available, make all squares available
    if (mAvailable.isEmpty()) {
      setAllAvailable();
    }
  }

  private void setAllAvailable() {
    for (int large = 0; large < 9; large++) {
      for (int small = 0; small < 9; small++) {
        Tile tile = mSmallTiles[large][small];
        if (tile.getOwner() == Tile.Owner.NEITHER)
          addAvailable(tile);
      }
    }
  }

  private void updateAllTiles() {
    mEntireBoard.updateDrawableState();
    for (int large = 0; large < 9; large++) {
      mLargeTiles[large].updateDrawableState();
      for (int small = 0; small < 9; small++) {
        mSmallTiles[large][small].updateDrawableState();
      }
    }
  }

  /**
   * Create a string containing the state of the game.
   */
  public String getState() {
    StringBuilder builder = new StringBuilder();
    builder.append(mLastLarge);
    builder.append(',');
    builder.append(mLastSmall);
    builder.append(',');
    for (int large = 0; large < 9; large++) {
      for (int small = 0; small < 9; small++) {
        builder.append(mSmallTiles[large][small].getOwner().name());
        builder.append(',');
      }
    }
    return builder.toString();
  }

  /**
   * Restore the state of the game from the given string.
   */
  public void putState(String gameData) {
    String[] fields = gameData.split(",");
    int index = 0;
    mLastLarge = Integer.parseInt(fields[index++]);
    mLastSmall = Integer.parseInt(fields[index++]);
    for (int large = 0; large < 9; large++) {
      for (int small = 0; small < 9; small++) {
        Tile.Owner owner = Tile.Owner.valueOf(fields[index++]);
        mSmallTiles[large][small].setOwner(owner);
      }
    }
    setAvailableFromLastMove(mLastSmall);
    updateAllTiles();
  }

  public static class WordScore {
    public String word = "";
    public int wordScore;

    public int getScore(Set<String> forbidden) {
      int t = (dic.contains(word) && !forbidden.contains(word)) ? wordScore : 0;
      if(t>bestWordScore){
        bestWordScore = t;
        bestWord = word;
      }
      return t;
    }

    public void clear() {
      wordScore = 0;
      word = "";
    }

    public void addLetter(char c) {
      if (c >= 'a' && c <= 'z') {
        word = word + c;
        wordScore += letterScores[c - 'a'];
      }
    }

    public void show() {
      ControlFragment.setScore();
      System.out.println(word + " " + getScore(wordSet));
    }
  }

}

