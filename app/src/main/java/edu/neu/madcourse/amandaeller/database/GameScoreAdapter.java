package edu.neu.madcourse.amandaeller.database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.amandaeller.R;

/**
 * Created by Amanda on 3/10/2017.
 */

public class GameScoreAdapter extends ArrayAdapter<GameScore> {
  public GameScoreAdapter(Context context, ArrayList<GameScore> gm) {
    super(context, 0, gm);
  }

  @Override
  public View getView(int position, View convertview, ViewGroup parent){
    final GameScore gs = getItem(position);

    if(convertview == null){
      convertview = LayoutInflater.from(getContext()).inflate(R.layout.score_list, parent, false);
    }

    TextView gameUserName = (TextView) convertview.findViewById(R.id.gameUserName);
    TextView gameScore = (TextView) convertview.findViewById(R.id.gameScore);
    TextView wordScore = (TextView) convertview.findViewById(R.id.wordScore);
    TextView bestWord = (TextView) convertview.findViewById(R.id.bestWord);
    TextView dateTime = (TextView) convertview.findViewById(R.id.datePlayed);

    gameUserName.setText(gs.username);
    gameScore.setText(gs.score);
    wordScore.setText(gs.wordScore);
    bestWord.setText(gs.bestWord);
    dateTime.setText(gs.datePlayed);

    return convertview;
  }
}
