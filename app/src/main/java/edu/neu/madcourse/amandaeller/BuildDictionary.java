package edu.neu.madcourse.amandaeller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Amanda on 2/1/2017.
 * This classed served the purpose of reading in the dictionary text file and converting into a
 * HashSet which is then serialized out and saved.
 * the Saved serialized file was copied into the assets folder and from there in the
 * MainActivity get deserialized and read into memory.
 *
 */

public class BuildDictionary {
  public static ArrayList<String> dic = new ArrayList<>();
  public static int[] pat = new int[784];
  public static int[][] adj = {{1,3,4},{0,2,3,4,5,},{1,4,5},{0,1,4,6,7},{0,1,2,3,5,6,7,8},{1,2,4,7,8},{3,4,7},{3,4,5,6,8},{4,5,7}};
  public static int[] bog = new int[9];
  public static int EMPTY = -1;
  public static int k = 0;

  public static void place(int slot, int val){
    if(bog[slot]== EMPTY) {
      bog[slot] = val;
      if (val == 8) {
        showBog();
      } else {
        for (int i = 0; i < adj[slot].length; i++) {
          place(adj[slot][i], val + 1);
        }
      }
      bog[slot] = EMPTY;
    }
  }

  public static void showBog(){
    int res = 0;
    for(int i = 0; i <9; i++){
      res = res*10 + bog[i];
    }
    pat[k] = res;
    k++;
  }


  public static void main(String[] args){
    System.out.println("building patterns");

    for(int i = 0; i <9; i++){
      bog[i] = EMPTY;
    }

    for(int i = 0; i <9; i++){
      place(i, 0);
    }

    for(int i = 0; i <pat.length; i++){
      System.out.println(i + ": " + pat[i]);
    }


//    InputStream input;
//    try {
//      String line;  // declared for use inside the while loop
//      BufferedReader in = new BufferedReader(new FileReader("C:/Users/Amanda/Downloads/wordlist.txt"));
//      while((line = in.readLine()) != null){
//        dic.add(line.trim().toLowerCase());} // add each line to the list
//      in.close();
//
//    } catch (IOException e) {
//      System.out.println("couldn't find file");
//      e.printStackTrace();
//    }
//
//    try {
//      FileOutputStream fos = new FileOutputStream("C:/Users/Amanda/Downloads/wordlist.dic");
//      ObjectOutputStream oos = new ObjectOutputStream(fos);
//      oos.writeObject(dic);
//      oos.close();
//
//    } catch (FileNotFoundException e) {
//      System.out.println("couldn't write file ");
//      e.printStackTrace();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
   // String foo = dic.contains("aalii")? "yes!": "no";
    //System.out.println(foo);


//    InputStream input;
//    try {
//      String line;  // declared for use inside the while loop
//      BufferedReader in = new BufferedReader(new FileReader("C:/Users/Amanda/Downloads/wordlist.txt"));
//      while((line = in.readLine()) != null){
//        line.trim();
//        if(line.length() == 9) {
//          dic.add(line.toLowerCase());
//        }
//      } // add each line to the list
//      in.close();
//
//    } catch (IOException e) {
//      System.out.println("couldn't find file");
//      e.printStackTrace();
//    }

    try {
      FileOutputStream fos = new FileOutputStream("C:/Users/Amanda/Downloads/patterns.dat");
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(pat);
      oos.close();

    } catch (FileNotFoundException e) {
      System.out.println("couldn't write file ");
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }



  }

}
