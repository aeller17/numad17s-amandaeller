/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/eband4 for more book information.
 ***/
package edu.neu.madcourse.amandaeller.scroggle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.amandaeller.R;
import edu.neu.madcourse.amandaeller.database.DatabaseActivity;
import edu.neu.madcourse.amandaeller.database.GameScore;


public class GameActivity extends Activity {
  public static final String KEY_RESTORE = "key_restore";
  public static final String PREF_RESTORE = "pref_restore";
  private static MediaPlayer mMediaPlayer;
  private Handler mHandler = new Handler();
  private GameFragment mGameFragment;
  public static GameScore finalScore;
  public static boolean gameOpen = false;

  private static DatabaseReference mDatabase;
  public static GameScore.ScoreBoard topScoreList = new GameScore.ScoreBoard("");
  public static GameScore.ScoreBoard topWordList = new GameScore.ScoreBoard("word");
  public static GameScore.ScoreBoard userScoreList = new GameScore.ScoreBoard("");
  public static GameScore.ScoreBoard userWordList = new GameScore.ScoreBoard("word");

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    gameOpen = true;
    setContentView(R.layout.activity_game);
    mGameFragment = (GameFragment) getFragmentManager()
        .findFragmentById(R.id.fragment_game);
    boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);
    if (restore) {
      String gameData = getPreferences(MODE_PRIVATE)
          .getString(PREF_RESTORE, null);
      if (gameData != null) {
        mGameFragment.putState(gameData);
      }
    }

    mDatabase = FirebaseDatabase.getInstance().getReference();
    mDatabase.child("Top10Word").addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        updateTopWords(dataSnapshot);
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {}});
    mDatabase.child("Top10Game").addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        updateTopGames(dataSnapshot);
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {}
    });
    Log.d("UT3", "restore = " + restore);
    toast();
  }

  private void toast() {
    String msg = "Check the Leader board to see what score you need to beat!";
    Toast.makeText(GameActivity.this, msg, Toast.LENGTH_SHORT).show();
  }

  public void restartGame() {mGameFragment.restartGame();}

  public void setAllColors() {mGameFragment.setAllColors();}

  public void phase2() {mGameFragment.phase2();}

  public static void onMute() {
    if (mMediaPlayer.isPlaying()) {
      mMediaPlayer.pause();
    } else {
      mMediaPlayer.start();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    mMediaPlayer = MediaPlayer.create(this, R.raw.bonkers);
    mMediaPlayer.setLooping(true);
    mMediaPlayer.start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    mHandler.removeCallbacks(null);
    mMediaPlayer.stop();
    mMediaPlayer.reset();
    mMediaPlayer.release();
    String gameData = mGameFragment.getState();
    getPreferences(MODE_PRIVATE).edit()
        .putString(PREF_RESTORE, gameData)
        .commit();
  }

  public void updateTopWords(DataSnapshot dataSnapshot) {
    topWordList.clear();
    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
      GameScore GS = snapshot.getValue(GameScore.class);
      topWordList.add(GS);
    }
  }

  public void updateTopGames(DataSnapshot dataSnapshot) {
    topScoreList.clear();
    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
      GameScore GS = snapshot.getValue(GameScore.class);
      topScoreList.add(GS);
    }
  }

  public static boolean canAdd(GameScore gm){
    return topWordList.willAdd(gm) || topScoreList.willAdd(gm);
  }

  public static void addScoreToTop10(GameScore gm) {
    if (topWordList.add(gm)){
      mDatabase.child("Top10Word").removeValue();
      for (GameScore gs : topWordList) {
        mDatabase.child("Top10Word").child(gs.username + "_" + gs.wordScore).setValue(gs);
      }
    }
    if (topScoreList.add(gm)) {
      mDatabase.child("Top10Game").removeValue();
      for (GameScore gs : topScoreList) {
        mDatabase.child("Top10Game").child(gs.username + "_" + gs.score).setValue(gs);
      }
    }
    DatabaseActivity.pushNotification();
  }

  public static void addUserScore(GameScore gm){
    userWordList.add(gm);
    userScoreList.add(gm);
  }

  public void endGame() {
    mMediaPlayer.pause();
    mGameFragment.endGame();

    if(!finalScore.bestWord.equals("")) {
      addUserScore(finalScore);
    }

    if(canAdd(finalScore)) {
      final EditText input = new EditText(this);
      new AlertDialog.Builder(this)
          .setTitle(R.string.submitScore_title)
          .setMessage(R.string.submitScore_msg)
          .setView(input)
          .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              String username = input.getText().toString();
              if(username.equals("")){username = "Anonymous";}
              finalScore.username = username;
              addScoreToTop10(finalScore);

              DatabaseActivity.isLeaderBoard = true;
              Intent i = new Intent(GameActivity.this, DatabaseActivity.class);
              startActivity(i);

            }
          })
          .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
          })
          .show();
    }
  }
}
