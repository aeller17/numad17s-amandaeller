package edu.neu.madcourse.amandaeller;

import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Amanda on 1/17/2017.
 */
public class AboutFragment extends Fragment {
  private String imei;

  @Override
  public void onCreate(Bundle saveInstanceState) {
    super.onCreate(saveInstanceState);
    setRetainInstance(true);
    getImei();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_about, container, false);

    View t = rootView.findViewById(R.id.phoneTextView);
    ((TextView)t).setText(imei);
    return rootView;
  }

  public void getImei(){
    TelephonyManager telephonyManager;
    imei = null;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      telephonyManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
      if(telephonyManager != null) {
        imei = telephonyManager.getDeviceId();
      }
    } else {
      imei = "Could not get device ID";
    }
    return;
  }

}
