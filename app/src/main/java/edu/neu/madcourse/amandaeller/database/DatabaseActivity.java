package edu.neu.madcourse.amandaeller.database;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import edu.neu.madcourse.amandaeller.R;
import edu.neu.madcourse.amandaeller.scroggle.GameActivity;

import static edu.neu.madcourse.amandaeller.scroggle.GameActivity.topScoreList;
import static edu.neu.madcourse.amandaeller.scroggle.GameActivity.topWordList;
import static edu.neu.madcourse.amandaeller.scroggle.GameActivity.userScoreList;
import static edu.neu.madcourse.amandaeller.scroggle.GameActivity.userWordList;

/**
 * Created by Amanda on 3/10/2017.
 */

public class DatabaseActivity extends Activity {
  private static final String SERVER_KEY = "key=AAAA8AkB2-8:APA91bEnfM66cPWA_cTlmdC9grmHaQeCcK5FT14-mWPaVCIweofi_yUsWVGG0877YizxQu85nOwm3rWoqHjaV6foiv8nhw-Be7VHWHUcyTAELo0nJWiE6oep8eGXMqNw6ZbIgEmRKPm1";

  public ArrayList<GameScore> displayList = new ArrayList<>();
  public static GameScoreAdapter listAdapter;

  public static Boolean isLeaderBoard = true;
  private Boolean orderByWord = false;
  private Button scoreBoardToggle;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

      if(isLeaderBoard){
        setContentView(R.layout.activity_board);
        if(!GameActivity.gameOpen){
          new AlertDialog.Builder(this)
              .setTitle(R.string.leaderboard_unavail_title)
              .setMessage(R.string.leaderboard_unavail)
              .setPositiveButton("Play Scroggle", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  Intent i = new Intent(DatabaseActivity.this, GameActivity.class);
                  startActivity(i);}
              })
              .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  closeActivity();
                }
              })
              .show();
        }
      } else{setContentView(R.layout.activity_scoreboard);}

    listAdapter = new GameScoreAdapter(this, displayList);

    scoreBoardToggle = (Button) findViewById(R.id.ScoreBoardToggle);
    scoreBoardToggle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        orderToggle();
        updateBoard();
      }
    });
    updateBoard();
  }

  private void closeActivity() {
    this.finish();
  }

  public void updateBoard() {
    displayList.clear();
    if (orderByWord) {
      if (isLeaderBoard) {
        for (GameScore g : topWordList) {
          displayList.add(g);
        }
      } else {
        for (GameScore g : userWordList) {
          displayList.add(g);
        }
      }
    } else {
      if (isLeaderBoard) {
        for (GameScore g : topScoreList) {
          displayList.add(g);
        }
      } else {
        for (GameScore g : userScoreList) {
          displayList.add(g);
        }
      }
    }
    listAdapter.notifyDataSetChanged();
    ListView lv = (ListView) findViewById(R.id.Scores);
    lv.setAdapter(listAdapter);
    //if(sub){addScoreToTop10(GameActivity.finalScore); sub = false;}
  }

  public void writeOutUserScore(View view) {
    String wordScores = "WordScores";
    String gameScores = "GameScores";

    FileOutputStream outStream;
    try {
      outStream = getApplicationContext().openFileOutput(wordScores, Context.MODE_PRIVATE);
      ObjectOutputStream oos = new ObjectOutputStream(outStream);
      oos.writeObject(GameActivity.userWordList);
      System.out.println("Writing word list: "+GameActivity.userWordList.size());
      oos.close();
      outStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    try {
      outStream = openFileOutput(gameScores, Context.MODE_PRIVATE);
      ObjectOutputStream oos = new ObjectOutputStream(outStream);
      oos.writeObject(GameActivity.userScoreList);
      System.out.println("Writing word list: "+GameActivity.userScoreList.size());
      oos.close();
      outStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void orderToggle() {
    if (orderByWord) {
      orderByWord = false;
      scoreBoardToggle.setText("Top 10 by Word Score");
    } else {
      orderByWord = true;
      scoreBoardToggle.setText("Top 10 by Game Score");
    }
  }

  public static void pushNotification() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        pushNot();
      }
    }).start();
  }

  public static void pushNot() {
    JSONObject jPayload = new JSONObject();
    JSONObject jNotification = new JSONObject();
    try {
      jNotification.put("title", "NUMAD17s-AmandaEller, Scroggle Score update");
      jNotification.put("body", "New score has been added to the top 10! Check out who it is");
      jNotification.put("sound", "default");
      jNotification.put("badge", "1");
      jNotification.put("click_action", "OPEN_ACTIVITY_1");

      // If sending to a single client
      jPayload.put("to", "/topics/ScroggleTop10");

      jPayload.put("priority", "high");
      jPayload.put("notification", jNotification);


      URL url = new URL("https://fcm.googleapis.com/fcm/send");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Authorization", SERVER_KEY);
      conn.setRequestProperty("Content-Type", "application/json");
      conn.setDoOutput(true);

      // Send FCM message content.
      OutputStream outputStream = conn.getOutputStream();
      outputStream.write(jPayload.toString().getBytes());
      outputStream.close();

      // Read FCM response.
      InputStream inputStream = conn.getInputStream();
      final String resp = convertStreamToString(inputStream);

      Handler h = new Handler(Looper.getMainLooper());
      h.post(new Runnable() {
        @Override
        public void run() {}
      });
    } catch (JSONException | IOException e) {
      e.printStackTrace();
    }
  }

  private static String convertStreamToString(InputStream is) {
    Scanner s = new Scanner(is).useDelimiter("\\A");
    return s.hasNext() ? s.next().replace(",", ",\n") : "";
  }
}
