package edu.neu.madcourse.amandaeller.slowLoris;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Based off the code that Professor Slaughter used in lecture 8
 */

public class AccelView extends View {
  private int mTime = 0;

  private float[] mAccelMatrix = new float[3];

  private Bitmap myBitmap;
  private Canvas myCanvas;

  private Paint xPaint = new Paint();
  private Paint yPaint = new Paint();
  private Paint zPaint = new Paint();

  private Paint bgPaint = new Paint();
  private Paint labelPaint = new Paint();

  private Trace trace = new Trace();

  private float STROKE_WIDTH = 4.0f;

  private int X_MED = 150;
  private int Y_MED = 250;
  private int Z_MED = 350;

  private int SCALE = 30;

  public AccelView(Context context) {
    super(context);
    createBuffer();
  }

  public AccelView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    setupPaint();

    // Allows us to create the image the size of the parent/containing layout.
    ViewTreeObserver viewTreeObserver = getViewTreeObserver();
    if (viewTreeObserver.isAlive()) {
      viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            getViewTreeObserver().removeOnGlobalLayoutListener(this);
          }
          createBuffer();
        }
      });
    }
    createBuffer();
  }

  private void setupPaint() {
    xPaint.setColor(Color.RED);
    xPaint.setStrokeWidth(STROKE_WIDTH);
    xPaint.setTextSize(28.0f);
    xPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    yPaint.setColor(Color.CYAN);
    yPaint.setStrokeWidth(STROKE_WIDTH);
    yPaint.setTextSize(28.0f);
    yPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    zPaint.setColor(Color.GREEN);
    zPaint.setStrokeWidth(STROKE_WIDTH);
    zPaint.setTextSize(28.0f);
    zPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

    bgPaint.setColor(Color.argb(255, 50, 50, 50));

    labelPaint.setColor(Color.WHITE);
    labelPaint.setStrokeWidth(3.0f);
  }

  public void setAccelMatrix(float[] newVals) {
    mTime++;

    if (mTime > getWidth()) {
      resetScreen();
    }

    if (trace.isNotFull()) {
      trace.buff[trace.nBuff][0] = newVals[0];
      trace.buff[trace.nBuff][1] = newVals[1];
      trace.buff[trace.nBuff][2] = newVals[2];
      trace.nBuff++;
    }

    updateValues(myCanvas, mAccelMatrix.clone(), newVals.clone(), mTime);

    mAccelMatrix = newVals.clone();
    invalidate();
  }

  protected void createBuffer() {
    int w = getWidth();
    int h = getHeight();

    myBitmap = Bitmap.createBitmap(((w == 0) ? 1 : w),
        ((h == 0) ? 1 : h), Bitmap.Config.ARGB_8888);
    myCanvas = new Canvas(myBitmap);
    myCanvas.drawRect(0, 0, w, h, bgPaint);

    myCanvas.drawText("X", 10, h, xPaint);
    myCanvas.drawText("Y", 35, h, yPaint);
    myCanvas.drawText("Z", 50, h, zPaint);
  }

  protected void resetScreen(){
    mTime = 0;
    createBuffer();
  }

  protected void resetTrace() {
    trace.nBuff = 0;
  }

  protected void updateValues(Canvas canvas, float[] oldVals, float[] newVals, int time) {
    if (trace.movingTooFast(8, 50)) {
//      xPaint.setColor(Color.YELLOW);
//      yPaint.setColor(Color.CYAN);
//      zPaint.setColor(Color.WHITE);
      resetScreen();
      resetTrace();
//    }else{
//      xPaint.setColor(Color.RED);
//      yPaint.setColor(Color.BLUE);
//      zPaint.setColor(Color.GREEN);
    }

    if (canvas != null && trace.isNotFull()) {
      canvas.drawLine(time - 1, X_MED + (oldVals[0] * SCALE), time, X_MED + (newVals[0] * SCALE), xPaint);
      canvas.drawLine(time - 1, Y_MED + (oldVals[1] * SCALE), time, Y_MED + (newVals[1] * SCALE), yPaint);
      canvas.drawLine(time - 1, Z_MED + (oldVals[2] * SCALE), time, Z_MED + (newVals[2] * SCALE), zPaint);

    } else if (canvas != null) {
      canvas.drawLine(time - 1, X_MED, time, X_MED, xPaint);
      canvas.drawLine(time - 1, Y_MED, time, Y_MED, yPaint);
      canvas.drawLine(time - 1, Z_MED, time, Z_MED, zPaint);
    }
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawBitmap(myBitmap, 0, 0, null);
  }

  public class Trace {
    private int nBuff = 0;
    private static final int N = 1000;
    private final float[][] buff;

    public Trace() {
      buff = new float[N][3];
    }

    public boolean isNotFull() {
      return nBuff < N;
    }

    public boolean movingTooFast(int n, float threshold) {
      if (nBuff < n) {
        return false; //false = smoothing movement, haven't jerked the phone around
      }
      float aveX = 0;
      float aveY = 0;
      float aveZ = 0;
      for (int i = nBuff - n; i < nBuff; i++) {
        aveX = aveX + buff[i][0];
        aveY = aveY + buff[i][1];
        aveZ = aveZ + buff[i][2];
      }
      aveX = aveX/n;
      aveY = aveY/n;
      aveZ = aveZ/n;
      float bd = 0;
      for(int i = nBuff - n; i< nBuff; i++ ){
        float x = Math.abs(buff[i][0] - aveX);
        float y = Math.abs(buff[i][1] - aveY);
        float z = Math.abs(buff[i][2] - aveZ);
        if(x>bd){bd = x*100;}
        if(y>bd){bd = y*100;}
        if(z>bd){bd = z*100;}
      }
      return bd>threshold;
    }
  }

}
